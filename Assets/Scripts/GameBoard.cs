﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class GameBoard : MonoBehaviour {

    //=========================================================================================

    public static GameBoard currentGameBoard = null;

	private int columns = 0;
	private int rows = 0;

	private Tile [,] mTiles; 

	[SerializeField] private List<GameObject> stonePrefab;
	private int nextPrefab = 0;
	private GameObject[] prefabQeue;

	[SerializeField] private RectTransform tilesTransform;
	[SerializeField] private RectTransform stonesTransform;

	private Tile tmpSourceTile = null;
	private Tile tmpTargetTile = null;
    
    //For effect
    public GameObject sparkPrefab;

    //=========================================================================================

    void Start () {
		
		// Only one GameBoard should be present in the scene.
		if (currentGameBoard == null) {
			currentGameBoard = this;
		} else if(currentGameBoard != this){
			Destroy (this.gameObject);
		}

		Tile [] allTheTiles = tilesTransform.GetComponentsInChildren<Tile> ();

		// Find out size of board needed. ----------

		foreach(Tile tmp in allTheTiles){
			BoardIndex tmpPos = PositionOnBoard (new Vector2(tmp.transform.localPosition.x, tmp.transform.localPosition.y));
			if (tmpPos.X >= columns)
				columns = tmpPos.X + 1; 
			if (tmpPos.Y >= rows)
				rows = tmpPos.Y + 1;

			tmp.MIndex = new BoardIndex(tmpPos);
		}

		// Initialize mTiles------------------------
		mTiles = new Tile[columns, rows];

		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				mTiles [i, j] = null;
			}
		}

		foreach (Tile tmp in allTheTiles) {
			if (mTiles [tmp.MIndex.X, tmp.MIndex.Y] == null)
				mTiles [tmp.MIndex.X, tmp.MIndex.Y] = tmp;
			else
				Destroy (tmp.gameObject);
		}
		//-------------------------------------------
		prefabQeue = stonePrefab.ToArray();
		nextPrefab = stonePrefab.Count; //Triggers prefabQeue shuffle
		FillGameBoardWithStones ();

        List<Tile> matches;
        if (CheckForMatchesOnGameBoard(out matches))
        {
            ReplaceStones(matches);
        }
        //-------------------------------------------
    }

	/// <summary>
	/// Iterate through the whole GameBoard and place stone to every tile that does not have one.
	/// </summary>
	private void FillGameBoardWithStones()
	{
		GameObject newStone;
		for (int i = 0; i < columns; i++) 
		{
			for (int j = 0; j < rows; j++) 
			{
				if (mTiles [i, j] != null) 
				{
					if (mTiles [i, j].mStone == null) 
					{
						newStone = Instantiate (GetNextStonePrefab (), stonesTransform) as GameObject;
						mTiles [i, j].mStone = newStone.GetComponent<Stone> ();
						newStone.transform.localPosition = mTiles [i, j].transform.localPosition;
					}
				}
			}
		}
	}

	/// <summary>
	/// Gets next <c>Stone</c> prefab in <c>prefabQeue</c>.
	/// Shuffles the qeue on overflow and starts from the begining.
	/// </summary>
	/// <returns>The next stone prefab.</returns>
	private GameObject GetNextStonePrefab(){
		nextPrefab++;

		if (nextPrefab >= stonePrefab.Count) {

			GameObject tmpPrefab = null;
			int randomIndex = 0;

			for(int i = 0 ; i < (stonePrefab.Count-1) ; i++ ){
				
				randomIndex = UnityEngine.Random.Range (i + 1, stonePrefab.Count-1);
				tmpPrefab = prefabQeue[i];
				prefabQeue [i] = prefabQeue [randomIndex];
				prefabQeue [randomIndex] = tmpPrefab;

			}
			nextPrefab = 0;
		}
			
		return prefabQeue [nextPrefab];
	}

    /// <summary>
    /// Replace stones at <c>tiles</c> with other random stones.
    /// </summary>
    /// <param name="tiles"></param>
    private void ReplaceStones(List<Tile> tiles) {

        for (int i = 0, j = tiles.Count; i < j; i++) {
            if (tiles[i].mStone != null) {
                Destroy(tiles[i].mStone.gameObject);
                (Instantiate(GetNextStonePrefab(), stonesTransform) as GameObject).GetComponent<Stone>().JumpTo(tiles[i]);
            }
        }
        //Debug.Log("Replaced: " + tiles.Count + " Tiles.");
    }


    //----------------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Iterates through all stones to find out if player can move some stones to make a match.
    /// </summary>
    /// <returns>Returns possible match on success, empty list otherwise.</returns>
    public List<Tile> FindPossibleMatch() {

        List<Tile> match = new List<Tile>();

        for (int i = 0 ; i < columns - 1 ; i++) {
            for (int j = 0 ; j < rows - 2 ; j++) {

                if(StartCheckVerticalRectangle(i, j, ref match))
                    return match;

                if (StartCheckHorizontalRectangle(i, j, ref match))
                    return match;
            }
        }

        return match;
    }

    private bool StartCheckVerticalRectangle(int x, int y, ref List<Tile> match) {

        BoardIndex bIndex = new BoardIndex(x, y);

        for (; bIndex.X < (x + 2); bIndex.X++)
        {
            if (IsStoneAtBoardIndex(bIndex))
            {
                match.Clear();
                match.Add(mTiles[bIndex.X, y]);
                if (CheckVerticalRectangle(x, y + 1, ref match))
                    return true;              
            }
        }

        match.Clear();
        return false;
    }

    private bool CheckVerticalRectangle(int x, int y, ref List<Tile> match)
    {

        BoardIndex bIndex = new BoardIndex(x, y);

        for (; bIndex.X < (x + 2); bIndex.X++)
        {
            if (IsStoneAtBoardIndex(bIndex))
            {
                if (match[match.Count-1].mStone.isSameType(mTiles[bIndex.X, y].mStone))
                {
                        match.Add(mTiles[bIndex.X, y]);
                        if (match.Count == Constants.MIN_MATCHING_TILES)
                            return true;

                        return CheckVerticalRectangle(x, y + 1, ref match);
                }
            }
        }

        return false;
    }

    private bool StartCheckHorizontalRectangle(int x, int y, ref List<Tile> match)
    {

        BoardIndex bIndex = new BoardIndex(x, y);

        for (; bIndex.Y < (y + 2); bIndex.Y++)
        {
            if (IsStoneAtBoardIndex(bIndex))
            {
                match.Clear();
                match.Add(mTiles[x, bIndex.Y]);
                if (CheckHorizontalRectangle(x+1, y, ref match))
                    return true;
            }
        }

        match.Clear();
        return false;
    }

    private bool CheckHorizontalRectangle(int x, int y, ref List<Tile> match)
    {

        BoardIndex bIndex = new BoardIndex(x, y);

        for (; bIndex.Y < (y + 2); bIndex.Y++)
        {
            if (IsStoneAtBoardIndex(bIndex))
            {
                if (match[match.Count - 1].mStone.isSameType(mTiles[x,bIndex.Y].mStone))
                {
                    match.Add(mTiles[x, bIndex.Y]);
                    if (match.Count == Constants.MIN_MATCHING_TILES)
                        return true;

                    return CheckHorizontalRectangle(x+1, y, ref match);
                }
            }
        }

        return false;
    }

    //----------------------------------------------------------------------------------------------------------------------------------


    /// <summary>
    /// Iterates through given <c>List</c> and moves <c>Stone</c>s to empty <c>Tile</c>s in direction of gravity.
    /// </summary>
    /// <param name="EmptyTiles"></param>
	private void FillInEmptyTiles (List<Tile> EmptyTiles){
		
		while (EmptyTiles.Count > 0) {

            if (EmptyTiles[0].mStone == null)
            {
                FillEmptyLine(EmptyTiles[0]);
            }

            EmptyTiles.RemoveAt(0);            
		}

	}


	private void FillEmptyLine(Tile empty){

		BoardIndex gravity = new BoardIndex(GameController.Controller.Gravity);
		BoardIndex invertedGravity = gravity * (-1);
		BoardIndex i = empty.MIndex + invertedGravity;
        BoardIndex nextEmptyTilePos;
        Tile currentEmptyTile = empty;

		//We shall start from the "lowest" Tile in order for new Stones to spawn correctly
		for(BoardIndex j = new BoardIndex(i); IsBoardIndexInBounds(j) ; j += gravity ){
			if (mTiles [j.X, j.Y] != null)
				if (mTiles [j.X, j.Y].mStone == null)
					currentEmptyTile = mTiles [j.X, j.Y];
		}

		i = currentEmptyTile.MIndex + invertedGravity;

        for (; currentEmptyTile != null ; i += invertedGravity)
        {
            if (IsBoardIndexInBounds(i))
            {
				// We look for another stone at GameBoard
				// If we find one we steal it and proclaim the tile we stole it from empty.
                if (mTiles[i.X, i.Y] != null)
                {
                    if (mTiles[i.X, i.Y].mStone != null)
                    {
                        currentEmptyTile.mStone = mTiles[i.X, i.Y].mStone;
                        currentEmptyTile.mStone.MoveToNewTile(currentEmptyTile);
                        mTiles[i.X, i.Y].mStone = null;

                    }
                }
            }
            else
            {
                //If there is no stone in opposite of gravity direction - we spawn a new one
                GameObject newStone = Instantiate(GetNextStonePrefab(), stonesTransform) as GameObject;
                currentEmptyTile.mStone = newStone.GetComponent<Stone>();
                newStone.transform.localPosition = PositionOnBoard(i);
                currentEmptyTile.mStone.MoveToNewTile(currentEmptyTile);
            }

            if (currentEmptyTile.mStone != null)
            {
                nextEmptyTilePos = currentEmptyTile.MIndex + invertedGravity;
                currentEmptyTile = null;

                //Skipping empty positions on board where are no Tiles
                for (; IsBoardIndexInBounds(nextEmptyTilePos) && (currentEmptyTile == null); nextEmptyTilePos += invertedGravity)
                {
                    currentEmptyTile = mTiles[nextEmptyTilePos.X, nextEmptyTilePos.Y];
                }
            }
        }
	}


	/// <summary>
	/// First phase of swithing <c>Stones</c>. <c>Stone</c> are moved only if they are next to each other. Expecting <c>Tiles</c> at which are <c>Stones</c> placed.
	/// </summary>
	/// <returns><c>true</c>, on success, <c>false</c> otherwise.</returns>
	/// <param name="sourceTile">Source tile.</param>
	/// <param name="targetTile">Target tile.</param>
	public bool AttemptToSwithStones(Tile sourceTile, Tile targetTile){

		tmpSourceTile = sourceTile;
		tmpTargetTile = targetTile;

		// We only switch stones if they don't move further than 1 tile
		if ((Mathf.Abs (targetTile.MIndex.X - sourceTile.MIndex.X) + Mathf.Abs (targetTile.MIndex.Y - sourceTile.MIndex.Y)) == 1) {

			Stone tmpStone = targetTile.mStone;
			sourceTile.mStone.MoveToNewTile (targetTile);
			tmpStone.MoveToNewTile (sourceTile);

			GameController.OnAnimEnd += FinishSwitchingStones;

			return true;
		}

		return false;
	}

	/// <summary>
	/// Second phase of switching stones. Used after finishing animation. 
	/// Removes matches when they occur, reverts stone switch otherwise.
	/// </summary>
	public void FinishSwitchingStones(){

		List<Tile> matchingTiles = new List<Tile>();
		GameController.OnAnimEnd -= FinishSwitchingStones;
        //Debug.Log("Finish stone switch");
		bool matchesFound = CheckForMatchesNextToTile (tmpTargetTile, ref matchingTiles);
		matchesFound |= CheckForMatchesNextToTile (tmpSourceTile, ref matchingTiles);

		if ( matchesFound ) {

			ScoreMatchingTiles (ref matchingTiles);

            GameController.OnAnimEnd += ScoreMatchesOnGameBoard;

            GameController.Controller.Move ();

		} else {
			//No matches found - switch back the stones.
			Stone tmpStone = tmpTargetTile.mStone;
			tmpSourceTile.mStone.MoveToNewTile (tmpTargetTile);
			tmpStone.MoveToNewTile (tmpSourceTile);

            GameController.Controller.ActionDenied();
		}
    }

	private bool CheckForMatchesNextToTile(Tile targetTile, ref List<Tile> theMatchingTiles){
	
		bool result = false;
		List <Tile> horizontalMatchingTiles = CheckForMatchesInDirection (targetTile, -1, 0);
		horizontalMatchingTiles.AddRange (CheckForMatchesInDirection (targetTile, 1, 0));

		List <Tile> verticalMatchingTiles = CheckForMatchesInDirection (targetTile, 0, -1);
		verticalMatchingTiles.AddRange (CheckForMatchesInDirection (targetTile, 0, 1));

		if (horizontalMatchingTiles.Count >= (Constants.MIN_MATCHING_TILES - 1)) {
			theMatchingTiles.AddRange (horizontalMatchingTiles);
			result = true;
		}

		if(verticalMatchingTiles.Count >= (Constants.MIN_MATCHING_TILES - 1)){
			theMatchingTiles.AddRange (verticalMatchingTiles);
			result = true;
		}

		if (result)
			theMatchingTiles.Add (targetTile);

		return result;
	}

	/// <summary>
	/// Iterates through tiles from given <c>targetTile</c> in direction X and Y. Checks for matches and stops on mismatch.
	/// </summary>
	/// <returns>Tiles with same stones as targetTile in given direction.</returns>
	/// <param name="targetTile">Starting point tile.</param>
	/// <param name="directionX">Direction x.</param>
	/// <param name="directionY">Direction y.</param>
	private List<Tile> CheckForMatchesInDirection(Tile targetTile, int directionX, int directionY){
		
		BoardIndex targetTileIndex = new BoardIndex(targetTile.MIndex);
		List<Tile> TilesWithMatchingStones = new List<Tile> ();

		targetTileIndex.X += directionX;
		targetTileIndex.Y += directionY;

		while (IsBoardIndexValid(targetTileIndex)){
			if (!mTiles [targetTileIndex.X, targetTileIndex.Y].mStone.isSameType (targetTile.mStone))
				break;

			TilesWithMatchingStones.Add (mTiles [targetTileIndex.X, targetTileIndex.Y]);
			targetTileIndex.X += directionX;
			targetTileIndex.Y += directionY;
		}

		return TilesWithMatchingStones;
	}

    /// <summary>
    /// Iterates through the whole GameBoard to find matching tiles.
    /// </summary>
    /// <returns><c>true</c>, if match was found, <c>false</c> otherwise.</returns>
    /// <param name="matchingTiles">Matching tiles.</param>
    public bool CheckForMatchesOnGameBoard(out List<Tile> matchingTiles)
    {

        matchingTiles = new List<Tile>();

        foreach (Tile currentT in mTiles)
        {
            if (currentT != null)
            {
                CheckForMatchesNextToTile(currentT, ref matchingTiles);
            }
        }

        if (matchingTiles.Count > 0)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Iterates through the whole GameBoard to find matching tiles.
    /// If matches was found it triggers scoring.
    /// </summary>
    /// <returns><c>true</c>, if match was found, <c>false</c> otherwise.</returns>
    /// <param name="matchingTiles">Matching tiles.</param>
    public bool ScoreMatchesOnGameBoard(out List<Tile> matchingTiles){
		
		matchingTiles = new List<Tile> ();

		foreach (Tile currentT in mTiles) {
			if (currentT != null) {
				CheckForMatchesNextToTile (currentT, ref matchingTiles);					
			}
		}

		if (matchingTiles.Count > 0) {
			ScoreMatchingTiles (ref matchingTiles);
            return true;
		}

        GameController.OnAnimEnd -= ScoreMatchesOnGameBoard;

        return false;
	}

	public void ScoreMatchesOnGameBoard(){
		List<Tile> dummy;
        ScoreMatchesOnGameBoard(out dummy);
	}

    /// <summary>
    /// Destroyes <c>Stone</c> at given <c>matchingTiles</c> and adds their value to Score.
    /// 
    /// </summary>
    /// <param name="matchingTiles"></param>
	public void ScoreMatchingTiles(ref List<Tile> matchingTiles){
		Tile currentT = null;

        GameController.Controller.StoneMatch();

		for (int i=0; i < matchingTiles.Count ;i++) {
			currentT = matchingTiles[i];

			if (currentT.mStone != null) {
                // Spawn effect
                (Instantiate(sparkPrefab, currentT.mStone.transform.parent) as GameObject).transform.position = currentT.mStone.transform.position;

				GameController.Controller.AddScore (currentT.mStone);
				Destroy (currentT.mStone.gameObject);
				currentT.mStone = null;
			} else {
				matchingTiles.RemoveAt (i);
				i--;
			}
		}

		FillInEmptyTiles (matchingTiles);
	}

    public void GetNativeGameBoardSize(out int width, out int height) {
        width = Mathf.CeilToInt(Constants.STONE_FIELD_SIZE * columns);
        height = Mathf.CeilToInt(Constants.STONE_FIELD_SIZE * rows);
    }

    //Helper methods for positioning on the game board-------------------------------------------------------

    public BoardIndex PositionOnBoard(Vector2 localPosition){
		BoardIndex finalPosition = new BoardIndex();

		finalPosition.X = Mathf.FloorToInt (localPosition.x/Constants.STONE_FIELD_SIZE);
		finalPosition.Y = Mathf.FloorToInt (localPosition.y/Constants.STONE_FIELD_SIZE);

		return finalPosition;
	}

	public Vector3 PositionOnBoard(BoardIndex index){
		Vector3 result = Vector3.zero;

		result.x = index.X * Constants.STONE_FIELD_SIZE;
		result.y = index.Y * Constants.STONE_FIELD_SIZE;

		return result;
	}

    public bool IsStoneAtBoardIndex(BoardIndex index)
    {
        int x = index.X, y = index.Y;

        if ((x < 0) || (x >= columns))
            return false;
        if ((y < 0) || (y >= rows))
            return false;

        if (mTiles[x, y] == null)
            return false;

        if (mTiles[x, y].mStone == null)
            return false;

        return true;
    }

    /// <summary>
    /// Determines whether this BoardIndex coords refer to an existing Tile.
    /// </summary>
    /// <param name="index">BoardIndex.</param>
    public bool IsBoardIndexValid(BoardIndex index)
    {
        int x = index.X, y = index.Y;

        if ((x < 0) || (x >= columns))
			return false;
		if ((y < 0) || (y >= rows))
			return false;

		if (mTiles [x, y] == null)
			return false;

		return true;
	}

	/// <summary>
	/// Determines whether this BoardIndex is wihtin the GameBoard bounds.
	/// </summary>
	/// <param name="index">BoardIndex.</param>
	public bool IsBoardIndexInBounds(BoardIndex index){

		if ((index.X < 0) || (index.X >= columns))
			return false;
		if ((index.Y < 0) || (index.Y >= rows))
			return false;

		return true;
	}

	//=========================================================================================
}
