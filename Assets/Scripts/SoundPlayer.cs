﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

    public static SoundPlayer cPlayer = null;

    [SerializeField]
    private AudioSource musicSource;
    [SerializeField]
    private AudioSource soundSource;

    private static bool musicOn = true;
    public static bool MusicOn {
        get { return musicOn; }
        set
        {
            musicOn = value;
            PlayerPrefs.SetInt("musicOn", value ? 1 : 0);
            cPlayer.musicSource.mute = !value;
        }
    }

    private int musicIndex = 0;

    private static bool soundOn = true;
    public static bool SoundOn
    {
        get { return soundOn; }
        set
        {
            soundOn = value;
            PlayerPrefs.SetInt("soundOn", value ? 1 : 0);
        }
    }

    // Audio clips
    [SerializeField]
    private AudioClip actionDeniedSound;
    [SerializeField]
    private AudioClip clickSound;
    [SerializeField]
    private AudioClip stonesMatchSound;
    [SerializeField]
    private AudioClip [] musicTracks;

    [SerializeField]
    private float musicPauseTime = 10f;
    private float nextMusicTrackTime = 0f;

    //=========================================================================================

    void Start ()
    {
        // Make this instance the one and only
        if (cPlayer == null)
        {
            cPlayer = this;
            DontDestroyOnLoad(this.gameObject);
        }else if(cPlayer != this){
            Destroy(this.gameObject);
        }

        //Load player sound preferences
        musicOn = PlayerPrefs.GetInt("musicOn",1) > 0 ? true : false ;
        soundOn = PlayerPrefs.GetInt("soundOn", 1) > 0 ? true : false ;

        GameController.OnActionDenied += playActionDeniedSound;

        GameController.OnStoneMatch += playStonesMatchSound;
        
    }

    void Update()
    {
        //Play music if enabled and nothing playing
        if(musicOn)
            if (Time.time > nextMusicTrackTime)
            {
                musicSource.clip = musicTracks[musicIndex %= musicTracks.Length];
                musicIndex++;
                musicSource.Play();
                nextMusicTrackTime = Time.time + musicSource.clip.length + musicPauseTime;
            }

    }

    void playActionDeniedSound()
    {
        if (soundOn)
        {
            soundSource.clip = actionDeniedSound;
            soundSource.Play();
        }
    }

    
    void playStonesMatchSound()
    {
        if (soundOn)
        {
            soundSource.clip = stonesMatchSound;
            soundSource.Play();
        }
    }

    void OnDestroy() {

        GameController.OnActionDenied -= playActionDeniedSound;
        GameController.OnStoneMatch -= playStonesMatchSound;
    }

    //=========================================================================================

}
