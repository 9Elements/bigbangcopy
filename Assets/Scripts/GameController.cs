﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour {

	private static GameController currentController = null;
    public static GameController Controller {
        get{
            if (currentController == null) {
                currentController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            }
            return currentController;
        }
    }
    
    //Game-Logic-----------------------------------------------------------------------------------------------

    [SerializeField] private BoardIndex gravity = new BoardIndex(0,-1);
	public BoardIndex Gravity { get { return new BoardIndex(gravity);} }

#if UNITY_ANDROID
    private float gravityRefreshTime = 0f;
#endif

	[SerializeField, Range(1,Constants.MAX_MOVES)] private int movesLeft = 25;

	private int score = 0;
	public int Score{ get{ return score; } }

    private int oldHighScore = 0;

    //UI-------------------------------------------------------------------------------------------------------

    [SerializeField]
    private Text highScoreText;
    private string highScoreLabel = "";

    [SerializeField]
    private Text scoreText;
    private string scoreLabel = "";

    [SerializeField]
    private Text movesText;
    private string movesLabel = "";

    // Selection mark highlights selected stone
    [SerializeField]
    private GameObject selectionMarkPrefab = null;
    private GameObject selectionMark = null;

    //Events---------------------------------------------------------------------------------------------------
    
    //Use folowing variables to drive waiting for the end of animations in game.
    public delegate void AnimationEndActions ();
	public static event AnimationEndActions OnAnimEnd;

	private static int animating = 0;
	public static bool Animating{
		get
		{
			return animating > 0;
		}

		set
		{ 
			if (value == true) {
				animating++;
			} else {
				animating--;

				if (animating == 0) {
					if (OnAnimEnd != null)
						OnAnimEnd ();
					
					currentController.RemoveSelectionMark ();
				}
				if (animating < 0)
					Debug.LogError ("Negative value of 'animating' variable of GameController !!! " + animating);
			}
		}
	}

    public delegate void ActionDeniedEvent();
    public static event ActionDeniedEvent OnActionDenied;

    public delegate void StoneMatchEvent();
    public static event StoneMatchEvent OnStoneMatch;

    public delegate void MoveEvent();
    public static event MoveEvent OnMove;

    //============================================================================================================

    void Awake(){
        
		if (currentController == null) {
			currentController = this;
            animating = 0;
        } else if(currentController != this){
			Destroy (this);		
		}

#if UNITY_ANDROID
        Screen.orientation = ScreenOrientation.Portrait;
#endif
    }

    void Start(){

        // Initialize texts

        if (scoreText != null) {
            scoreLabel = Lang.GetString(scoreText.gameObject.name);
            scoreText.text = scoreLabel + "\n" + score.ToString ();
		} else {
			Debug.LogError ("GameController is missing Score text !");
		}

		if (movesText != null) {
            movesLabel = Lang.GetString(movesText.gameObject.name);
            movesText.text = movesLabel + "\n" + movesLeft.ToString ();
		} else {
			Debug.LogError ("GameController is missing Moves text !");
		}


        oldHighScore = PlayerPrefs.GetInt("HighScore/" + SceneManager.GetActiveScene().name, 0);

        if (highScoreText != null)
        {
            highScoreLabel = Lang.GetString(highScoreText.gameObject.name);
            highScoreText.text = highScoreLabel + "\n" + oldHighScore.ToString();
        }
        else
        {
            Debug.LogError("GameController is missing highScoreText !");
        }

    }



	void Update () {

        if (Input.GetKeyUp(KeyCode.Escape))
            QuitGame();

#if UNITY_ANDROID
        if (animating != 0)
            return;
        //Control gravity by accelerometer
        if (Time.time > gravityRefreshTime)
        {
            Vector3 acc = Input.acceleration;
            if ((acc.x * acc.x) > (acc.y * acc.y))
            {
                SetGravity((int)Mathf.Sign(acc.x), 0);
            }
            else
            {
                SetGravity(0, (int)Mathf.Sign(acc.y));
            }
            gravityRefreshTime += 0.3f;
        }

#endif
    }


    //Selection-mark-management---------------------------------------------------

    public GameObject GetSelectionMark()
    {
		if (selectionMark != null) {
			return selectionMark;
		} else {

			if (selectionMarkPrefab != null){
				return selectionMark = Instantiate (selectionMarkPrefab) as GameObject;
			} else {
				Debug.LogError ("Game controller is mising selection mark prefab !!!");
			}
		}

		return null;
	}

	public void RemoveSelectionMark()
    {
		if (selectionMark != null) {
			Destroy (selectionMark);
			selectionMark = null;
		}
	}
    //----------------------------------------------------------------------------

	public void AddScore(Stone matchedStone){
		score += matchedStone.Value;

		if (scoreText != null) {
			scoreText.text = scoreLabel + "\n" + score.ToString ();
		}
	}

    //-------------------------------------------------------------
    //Driven by accelerometer
    private void SetGravity(int X, int Y)
    {
        gravity = new BoardIndex(X, Y);
    }
    //Driven by button
    public void SetGravityUp()
    {
        gravity = new BoardIndex(0, 1);
    }
    
    public void SetGravityRight()
    {
        gravity = new BoardIndex(1, 0);
    }

    public void SetGravityDown()
    {
        gravity = new BoardIndex(0, -1);
    }

    public void SetGravityLeft()
    {
        gravity = new BoardIndex(-1, 0);
    }
    
    //-------------------------------------------------------------

    //Event triggers---------------------
    public void ActionDenied()
    {
        if(OnActionDenied != null)
            OnActionDenied();
    }
    
    public void StoneMatch()
    {
        if (OnStoneMatch != null)
            OnStoneMatch();
    }

    //Indicates one turn
    public void Move()
    {
        if (OnMove != null)
            OnMove();

        movesLeft--;
        if (movesLeft <= 0)
            GameOver();

        if (movesText != null)
        {
            movesText.text = movesLabel + "\n" + movesLeft.ToString();
        }
    }
    //------------------------------------

    private void GameOver()
    {
        bool playerWon = true;

        if (movesLeft == 0)
        {
            if (score > oldHighScore) {
                //Sets new highscore if player beated old one
                PlayerPrefs.SetInt("HighScore/" + SceneManager.GetActiveScene().name, score);

                if (highScoreText != null)
                {
                    oldHighScore = score;
                    highScoreText.text = highScoreLabel + "\n" + score.ToString();
                }
            }

            if (playerWon)
            {
                //Permanently unlocks next level
                PlayerPrefs.SetInt(LevelManager.GetNextLevelName(), 1);
            }

            //GameOverMenu.Menu.ShowGameOverMenu(score == oldHighScore, score, oldHighScore);
            GameOverMenu.Menu.ShowGameOverMenu(playerWon, score, oldHighScore);
        }
    }
    
    public void MenuPopUp() {

        if (InGameMenu.cMenu != null) {
            InGameMenu.cMenu.MenuPopUp();
        } else {
            Debug.LogError("Missing InGameMenu !");
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    //============================================================================================================
}
