﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;


//===================================================================
/// <summary>
/// Use folowing class for XML serialization
/// </summary>

public class Level
{
    [XmlAttribute("name")]
    public string levelName;
}

[XmlRoot("LevelOrder")]
public class LevelOrder
{
    [XmlArray("Levels")]
    [XmlArrayItem("level")]
    public List<Level> levelNames;
}

//===================================================================

public class LevelManager : MonoBehaviour {

    private static LevelManager lvlManager = null;
    public static LevelManager LvlManager {
        get {
            if (lvlManager == null)
            {
                lvlManager = new GameObject().AddComponent<LevelManager>();
                lvlManager.Start();
            }
            return lvlManager;
        }
    }

    public List<Level> sceneNames;

    //===============================================================

    void Awake() {

        if (lvlManager == null)
        {
            lvlManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (lvlManager != this)
        {
            Destroy(this.gameObject);
        }

        TextAsset _xml = Resources.Load<TextAsset>("Levels.xml");

        XmlSerializer serializer = new XmlSerializer(typeof(LevelOrder));

        StringReader reader = new StringReader(_xml.text);

        sceneNames = (serializer.Deserialize(reader) as LevelOrder).levelNames;

        reader.Close();
    }

	void Start () {


        
	}

    public static void LoadNextLevel() {

        int nextSceneIndex = 0, sceneCount = LvlManager.sceneNames.Count;
        string currentLvlName = SceneManager.GetActiveScene().name;

        for (int i = 0; i < sceneCount; i++)
        {
            if (lvlManager.sceneNames[i].levelName == currentLvlName)
            {
                nextSceneIndex = i+1;
                break;
            }
        }

        if (nextSceneIndex < sceneCount)
        {
            SceneManager.LoadScene(lvlManager.sceneNames[nextSceneIndex].levelName);
        } else {
            if (sceneCount == 0)
            {
                Debug.LogError("Something wrong with XML");
            }
            else
            {
                SceneManager.LoadScene(lvlManager.sceneNames[0].levelName);
            }
        }
        
    }

    public static string GetNextLevelName() {

        int nextSceneIndex = 0, sceneCount = LvlManager.sceneNames.Count;
        string currentLvlName = SceneManager.GetActiveScene().name;

        for (int i = 0; i < sceneCount; i++)
        {
            if (lvlManager.sceneNames[i].levelName == currentLvlName)
            {
                nextSceneIndex = i + 1;
                break;
            }
        }

        if (nextSceneIndex < sceneCount)
        {
            return lvlManager.sceneNames[nextSceneIndex].levelName;
        }
        else
        {
            return lvlManager.sceneNames[0].levelName; 
        }


        
    }

    //===============================================================
}
