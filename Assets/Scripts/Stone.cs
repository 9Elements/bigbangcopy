﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stone : MonoBehaviour {

    //Stone parameters
	[SerializeField, Range(1, Constants.MAX_STONE_TYPES)] private int type = 1;
	public int Type{get{ return type;}}

	[SerializeField, Range(0, Constants.MAX_STONE_VALUE)] private int value = 100;
	public int Value{ get{ return value;}}

    //Movement variables
	private bool moveTowardsTarget = false;
	private Vector3 targetPosition;
	private Vector3 velocity = Vector3.zero;
    private Vector3 direction;


    public Vector3 TargetPosition { get{return targetPosition;} }


    //================================================================================================

	void Start () {
        //Corrections
		if(!moveTowardsTarget)
			targetPosition = transform.position;
		
		transform.localScale = Vector3.one;
	}

	void Update () {

		if (moveTowardsTarget)
        {
			direction = targetPosition - transform.localPosition;
			velocity += direction.normalized * Constants.DEFAULT_GRAVITY_MPS * Time.deltaTime;

			if (velocity.sqrMagnitude > direction.sqrMagnitude)
            {
				transform.localPosition = targetPosition;
				velocity = new Vector3(0f,0f,0f);

				moveTowardsTarget = false;
				GameController.Animating = false;
			} else {
				transform.localPosition += velocity;
			}
		}

	}

	/// <summary>
	/// Jumps to <c>newTile</c> setting its reference to <c>this</c>.
	/// </summary>
	/// <param name="newTile">New tile.</param>
	public void JumpTo(Tile newTile){
		transform.localPosition = newTile.transform.localPosition;
		newTile.mStone = this;
	}

    /// <summary>
    /// Triggers a movement to <c>newTile</c> and assigns its own reference to newTile.mStone
    /// </summary>
    /// <param name="newTile"></param>
	public void MoveToNewTile(Tile newTile){
		
		targetPosition = newTile.transform.localPosition;
		newTile.mStone = this;

		if (!moveTowardsTarget) {
			moveTowardsTarget = true;
			GameController.Animating = true;
		}
	}

	public bool isSameType(Stone b){
		return Type == b.Type;
	}

	void OnDestroy(){

        if (moveTowardsTarget)
			GameController.Animating = false;
       
	}

    //================================================================================================

}
