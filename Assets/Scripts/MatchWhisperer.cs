﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Visualizes possible moves that player can make to score.
/// </summary>
public class MatchWhisperer : MonoBehaviour {

    private float nextWhisperTime = 0f;
    
    /// <summary>
    /// Interval has to be longer than the stone fall, otherwise will not work properly
    /// </summary>
    [SerializeField, Range(1f, 20)]
    private float whisperInterval = 5f;
    public bool whisper = true;

    private List<Tile> lastPossibleMatch;

    [SerializeField]
    private Color highlightColor;
    [SerializeField]
    private Color defaultColor;

    //---------------------------------------------------------------------------------------------------------

    void Start () {
        lastPossibleMatch = new List<Tile>();
        nextWhisperTime = Time.time + whisperInterval;

        GameController.OnMove += RefreshWhisperTime;
	}
	
	
	void Update () {

        if (!whisper)
            return;

        if (Time.time > nextWhisperTime)
        {
            if (GameController.Animating)
                RefreshWhisperTime();

            SetColorToTiles(defaultColor);
            lastPossibleMatch.Clear();

            lastPossibleMatch = GameBoard.currentGameBoard.FindPossibleMatch();
            SetColorToTiles(highlightColor);

            nextWhisperTime = Mathf.Infinity;
        }

	}

    private void SetColorToTiles(Color newColor) {

        if (lastPossibleMatch.Count >= Constants.MIN_MATCHING_TILES)
        {
            Image currentImg = null;

            for (int i = 0; i < lastPossibleMatch.Count; i++)
            {
                currentImg = lastPossibleMatch[i].gameObject.GetComponent<Image>();

                if (currentImg != null)
                    currentImg.color = newColor;
            }
        }
    }

    public void RefreshWhisperTime() {
        nextWhisperTime = Time.time + whisperInterval;

        SetColorToTiles(defaultColor);
        lastPossibleMatch.Clear();
    }

    void OnDestroy() {
        GameController.OnMove -= RefreshWhisperTime;
    }

    //---------------------------------------------------------------------------------------------------------
}
