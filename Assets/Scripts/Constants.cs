﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour {

	public const float DEFAULT_GRAVITY_MPS = 9.83f;
	public const float STONE_FOLLOW_SPEED = 0.1f;
	public const float STONE_FIELD_SIZE = 64f;

	public const int MAX_STONE_TYPES = 20;
	public const int MAX_STONE_VALUE = 10000;

	public const int MAX_MOVES = 300;


	public const int MIN_MATCHING_TILES = 3;
    
}
