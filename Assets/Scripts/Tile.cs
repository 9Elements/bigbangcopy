﻿using UnityEngine;
using System.Collections;

/// <summary>
/// <c>Tile></c> is a part of <c>GameBoard</c> that carries <c>Stone</c>. 
/// It is used for interaction between stones and the player.
/// </summary>
public class Tile : MonoBehaviour {

	public static Tile prevSelectedTile = null;
	public static Tile selectedTile = null;
	public static Tile onPointerEnter = null;

	private BoardIndex mIndex;
	public Stone mStone = null;

	///This property resolves positioning on GameBoard
	public BoardIndex MIndex{
		get{ return mIndex;}
		set{
			mIndex = value;
			transform.localPosition =  new Vector3( mIndex.X * Constants.STONE_FIELD_SIZE, mIndex.Y * Constants.STONE_FIELD_SIZE, 0f );
		}
	}

	//===============================================================

	void Start () {
		if (mStone != null)
			mStone.transform.localPosition = transform.localPosition;
	}

	public void SetPointerDown(){
		//When stones are moving, player should not be able to manipulate them.
		if (GameController.Animating) {
			Debug.Log ("Animating!!!");
			return;
		}

		prevSelectedTile = selectedTile;
		selectedTile = this;

		//If different tiles were selected, we attempt the switch
		if ((prevSelectedTile != null) && (prevSelectedTile != selectedTile)) {
		
			GameBoard.currentGameBoard.AttemptToSwithStones (prevSelectedTile, selectedTile);
			ResetSelection ();
		
		} else {
			// Marking selected stone
			GameObject selectionMark = GameController.Controller.GetSelectionMark ();

			if (prevSelectedTile == selectedTile) {
				Destroy (selectionMark);
			} else {
				selectionMark.transform.SetParent (selectedTile.mStone.transform);
				selectionMark.transform.localPosition = Vector3.zero;
			}	
		}
		//Debug.Log ("PointerDown called");
	}

	public void SetPointerEnter(){
		onPointerEnter = this;

		//Try to switch stones when player is dragging
		if (selectedTile != null) {
			if (Input.GetMouseButton (0) || Input.GetMouseButton (1)) {
				GameBoard.currentGameBoard.AttemptToSwithStones (selectedTile, onPointerEnter);
				selectedTile = null;
			}
		}
	}

	public void ResetSelection(){
		prevSelectedTile = null;
		selectedTile = null;
	}

	//===============================================================

}
