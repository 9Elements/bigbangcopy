﻿using UnityEngine;
using System.Collections;

public class BoardFitScreen : MonoBehaviour {

    private int lastScreenWidth = 0;
    private int BoardWidth, BoardHeight;
    private RectTransform mTransform;

    private float defaultWidth;

    void OnEnable()
    {
        GameBoard.currentGameBoard.GetNativeGameBoardSize(out BoardWidth, out BoardHeight);
        BoardWidth += Mathf.CeilToInt(Constants.STONE_FIELD_SIZE);
        mTransform = GetComponent<RectTransform>();
        defaultWidth = mTransform.sizeDelta.x;

        //Debug.Log("Native game board size (px) width: " + BoardWidth + " height: " + BoardHeight);
    }

	void Update ()
    {
        if (Screen.width != lastScreenWidth)
        {
            //Refresh size
            Vector3 newScale = Vector3.zero;
            
            newScale.x = Mathf.Clamp(BoardWidth, 1, Screen.width) / (BoardWidth * 1.0f);
            newScale.y = newScale.x;

            mTransform.localScale = newScale;
            lastScreenWidth = Screen.width;


            //Refresh position
            Vector2 newPos = mTransform.sizeDelta;

            newPos.x = Mathf.Clamp(lastScreenWidth - BoardWidth, 0, Mathf.Infinity) + defaultWidth;
            mTransform.sizeDelta = newPos;
        }
	}

}
