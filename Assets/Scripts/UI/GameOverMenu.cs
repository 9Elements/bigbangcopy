﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOverMenu : MonoBehaviour {

    private static GameOverMenu menu = null;
    public static GameOverMenu Menu {
        get {
            if (menu == null)
            {
                 menu = GameObject.FindGameObjectWithTag("GameOverMenu").GetComponent<GameOverMenu>();
            }

            return menu;
        }
    }
    
    //Menu-setup---------------------------------------------------------------------------------------------

    [SerializeField]
    private GameObject levelCompletedText;
    [SerializeField]
    private GameObject levelIncompleteText;

    [SerializeField]
    private GameObject TryAgainButton;
    [SerializeField]
    private GameObject NextLevelButton;

    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text highScoreText;


    //======================================================================================================

    void Start () {
        if (menu == null)
        {
            menu = this;
        }
        else if (menu != this)
        {
            Destroy(this.gameObject);
        }

        InitMenu();

        this.gameObject.SetActive(false);
    }
    
    void InitMenu()
    {
        
        levelCompletedText.SetActive(false);
        levelIncompleteText.SetActive(false);
        TryAgainButton.SetActive(false);
        NextLevelButton.SetActive(false);
    }

    public void ShowGameOverMenu(bool playerWon, int score, int highScore)
    {
        this.gameObject.SetActive(true);

        if (playerWon)
        {
            levelCompletedText.SetActive(true);
            NextLevelButton.SetActive(true);
        }
        else
        {
            levelIncompleteText.SetActive(true);
            TryAgainButton.SetActive(true);
        }
		scoreText.text = Lang.GetString("TL_YourScore") + "\n" + score.ToString();
		highScoreText.text = Lang.GetString("TL_YourHighestScore") + "\n" + highScore.ToString();

    }

    public void NextLevel() {

        LevelManager.LoadNextLevel();

    }

	public void LoadLocalLevel(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	public void RestartLevel()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

    //======================================================================================================

}
