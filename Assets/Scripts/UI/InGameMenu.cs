﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


/// <summary>
/// Add this class to ingame menu UI root.
/// </summary>
public class InGameMenu : MonoBehaviour {

    public static InGameMenu cMenu = null;

    [SerializeField]
    private GameObject SoundDisabledImage;
    [SerializeField]
    private GameObject MusicDisabledImage;
    [SerializeField]
    private bool HideOnStartUp = true;

    //===============================================================================

    void Start() {
        if (cMenu == null)
        {
            cMenu = this;
        }else if (cMenu != this){
            Destroy(this);
        }

        if(HideOnStartUp)
            Resume();
    }

    // Expecting to be in root of menu. Will not work properly if it's not.
    public void MenuPopUp()
    {
        this.gameObject.SetActive(true);
    }

    // Expecting to be in root of menu. Will not work properly if it's not.
    public void Resume()
    {
        this.gameObject.SetActive(false);
    }

    public void ToggleSound()
    {
        SoundPlayer.SoundOn = !SoundPlayer.SoundOn;

        if (SoundDisabledImage != null)
            SoundDisabledImage.SetActive(!SoundPlayer.SoundOn);
    }

    public void ToggleMusic()
    {
        SoundPlayer.MusicOn = !SoundPlayer.MusicOn;

        if(MusicDisabledImage != null)
            MusicDisabledImage.SetActive(!SoundPlayer.MusicOn);
    }

    public static void LoadLevel( string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadLocalLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    //===============================================================================
}
