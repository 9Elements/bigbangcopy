﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attach this script to an image you want to rotate when changing gravity.
/// Call RotateGravity() method to rotate gravity and the image clockwise.
/// At Android gravity is controlled by accelerometer, so this image will only show direction of gravity.
/// </summary>
public class GravityRotateButton : MonoBehaviour {
    
    private float targetAngle = 0;
    private bool rotating = false;

#if !UNITY_ANDROID
    private float maxAnimTime = 0.7f;
#endif
    private float animEndTime = 0f;

    private BoardIndex currentGravity;

#if UNITY_ANDROID
    private BoardIndex prevGravity;
#endif

    //============================================================================================================

#if UNITY_ANDROID
    void Start() {
        prevGravity = GameController.Controller.Gravity;
    }
#endif

    void Update()
    {
#if UNITY_ANDROID
        //At Android gravity is controlled by accelerometer, therefore this button should only show direction of gravity
        currentGravity = GameController.Controller.Gravity;

        if ((prevGravity != currentGravity) && !rotating) {

            if (currentGravity == new BoardIndex(0, -1))
            {
                targetAngle = 0;
            }
            else if (currentGravity == new BoardIndex(-1, 0))
            {
                targetAngle = -90;
            }
            else if (currentGravity == new BoardIndex(0, 1))
            {
                targetAngle = -180;
            }
            else if (currentGravity == new BoardIndex(1, 0))
            {
                targetAngle = -270;
            }

            rotating = true;
            prevGravity = currentGravity;
        }
#endif
        // Scripted rotation animation
        if (rotating)
        {

            transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.LerpAngle(transform.localRotation.eulerAngles.z, targetAngle, 0.1f));
            if (Time.time > animEndTime)
            {
                transform.localRotation = Quaternion.Euler(0f, 0f, targetAngle);
                rotating = false;
            }
        }
    }


    /// <summary>
    /// Turns gravity 90 degrees clockwise.
    /// </summary>
    public void RotateGravity() {

#if UNITY_STANDALONE_WIN

        if (rotating)
            return;

        if (GameController.Animating)
        {
            GameController.Controller.ActionDenied();
            return;
        }

        currentGravity = GameController.Controller.Gravity;

        if (currentGravity == new BoardIndex(0, -1))
        {
            GameController.Controller.SetGravityLeft();
            targetAngle = -90;
        }
        else if (currentGravity == new BoardIndex(-1, 0))
        {
            GameController.Controller.SetGravityUp();
            targetAngle = -180;
        }
        else if (currentGravity == new BoardIndex(0, 1))
        {
            GameController.Controller.SetGravityRight();
            targetAngle = -270;
        }
        else if (currentGravity == new BoardIndex(1, 0))
        {
            GameController.Controller.SetGravityDown();
            targetAngle = 0;
        }

        animEndTime = Time.time + maxAnimTime;
        rotating = true;

#endif

    }

    //============================================================================================================
}
