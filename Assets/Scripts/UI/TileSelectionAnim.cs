﻿using UnityEngine;
using System.Collections;

public class TileSelectionAnim : MonoBehaviour {

	public Vector3 rotationInDegreesPerFrame = Vector3.zero;

	void Start(){
		transform.localScale = Vector3.one;
	}

	void Update () {
		transform.Rotate(rotationInDegreesPerFrame);
	}
}
