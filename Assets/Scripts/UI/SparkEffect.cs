﻿using UnityEngine;
using System.Collections;

public class SparkEffect : MonoBehaviour {

    [SerializeField]
    private AnimationCurve scaleOverTime;

    [SerializeField]
    private float animationTime = 1f;
    private float animationStartTime = 0f;

    [SerializeField]
    private float rotationPerSecond = 120f; //degrees

    void Start() {
        animationStartTime = Time.time;
    }

	// Update is called once per frame
	void Update () {

        if (animationStartTime + animationTime > Time.time)
        {
            GetComponent<RectTransform>().localScale = Vector3.one * scaleOverTime.Evaluate(Time.time - animationStartTime);
            GetComponent<RectTransform>().localRotation *= Quaternion.Euler(0f,0f, rotationPerSecond * Time.deltaTime);
        }
        else
        {
            Destroy(this.gameObject);
        }
	}
}
