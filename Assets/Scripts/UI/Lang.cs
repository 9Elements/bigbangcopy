﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;



public class Lang : MonoBehaviour
{
	private static Hashtable dictionary;
	private static XmlDocument xml;
    
    private static bool initialized = false;

    public const string playerPrefsLanguageKey = "Lang";
    public const string defaultLanguage = "English";

    /// <summary>
    /// The language XML file path. Place it in /Resources/Lang directory.
    /// </summary>
    public const string LangPath = "Lang/";

    //============================================================================================

    void Awake(){

		Initialize ();        
	}

	public static void Initialize(){

        if (!initialized)
        {
            xml = new XmlDocument();

            //Init current language 
            if (!PlayerPrefs.HasKey(playerPrefsLanguageKey))
                PlayerPrefs.SetString(playerPrefsLanguageKey, Application.systemLanguage.ToString());

            string playersLanguage = PlayerPrefs.GetString(playerPrefsLanguageKey, defaultLanguage);

            Debug.Log("playersLanguage: " + playersLanguage );

            //Load language
			TextAsset xmlLang  = Resources.Load(LangPath + playersLanguage) as TextAsset;

            if (xmlLang == null)
            {
                Debug.Log("Missing players language, loading default language.");
                xmlLang = Resources.Load(LangPath + defaultLanguage) as TextAsset;
            }
            
            if (xmlLang == null)
            {
                Debug.LogError("Missing default language !!!");
                return;
            }

            LoadDictionary(xmlLang);

            initialized = true;
		}
	}

    public static void LoadDictionary(TextAsset xmlLang)
    {
        xml.LoadXml(xmlLang.text);

        dictionary = new Hashtable();
        XmlElement element = xml.DocumentElement["Dictionary"];
        if (element != null)
        {
            IEnumerator elemEnum = element.GetEnumerator();
            while (elemEnum.MoveNext())
            {
                XmlElement xmlItem = (XmlElement)elemEnum.Current;
                dictionary.Add(xmlItem.GetAttribute("name"), xmlItem.InnerText);
            }
        }
    }
    	
	public static string GetString (string name) {

		if (!dictionary.ContainsKey(name)) {
			Debug.LogError("The specified string does not exist: " + name);

			return "";
		}

        

		return (string)dictionary[name];
	}

    //============================================================================================
}

