﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class LevelButtonGenerator : MonoBehaviour {

    [SerializeField]
    private GameObject levelButtonPrefab;

    public int levelCount;

    public AnimationCurve pathX;
    public AnimationCurve pathY;

    [SerializeField, Range(0f,-1000f)]
    int selectionLeft = -200;
    [SerializeField, Range(0f, 1000f)]
    int selectionRight = 200;
    [SerializeField, Range(0f, -1000f)]
    int selectionBottom = -200;
    [SerializeField, Range(0f, 1000f)]
    int selectionTop = 200;
    
    //========================================================================================================

    void Start () {
        
        List<Level> levels = LevelManager.LvlManager.sceneNames;

        int selectionWidth = Mathf.Abs(selectionLeft) + Mathf.Abs(selectionRight);
        int selectionHeight = Mathf.Abs(selectionBottom) + Mathf.Abs(selectionTop);

        //Instantiate level buttons and assign proper levels to them
        // i is 1 because level 0 is default menu
        for (int i = 1; i < levels.Count; i++)
        {
            GameObject newLvlButton = Instantiate(levelButtonPrefab, this.transform) as GameObject;
            string argument = levels[i].levelName;

            newLvlButton.GetComponent<Button>().onClick.AddListener( () => { InGameMenu.LoadLevel(argument); } );

            newLvlButton.GetComponent<RectTransform>().localPosition = new Vector3(
                pathX.Evaluate((float)i / levels.Count) * selectionWidth + selectionLeft,
                pathY.Evaluate((float)i / levels.Count) * selectionHeight + selectionBottom, 
                0f);

            //If level is not unlocked, we disable the button so player cannot access it
            if (i > 1)
                newLvlButton.GetComponent<Button>().interactable = (PlayerPrefs.GetInt(argument, 0) != 0);
            
        }
	}

    //========================================================================================================

}
