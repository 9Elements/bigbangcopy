﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextLangManager : MonoBehaviour {

    //==================================================================

    void Awake () {
		Lang.Initialize ();

        foreach (Text tmp in GetComponentsInChildren<Text>())
        {
            tmp.text = Lang.GetString(tmp.gameObject.name);
        }   
         
	}

	//==================================================================
}
