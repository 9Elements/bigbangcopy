﻿using UnityEngine;
using System.Collections;

/// <summary>
/// BoardIndex helps with orientation on GameBoard.
/// </summary>
[System.Serializable]
public class BoardIndex
{
    //---------------------------------------

    public int X;
    public int Y;

    //---------------------------------------

    public BoardIndex()
    {
        X = -1;
        Y = -1;
    }

    public BoardIndex(int a, int b)
    {
        X = a;
        Y = b;
    }

    public BoardIndex(BoardIndex b)
    {
        X = b.X;
        Y = b.Y;
    }

    public static BoardIndex operator +(BoardIndex a, BoardIndex b)
    {
        return new BoardIndex(a.X + b.X, a.Y + b.Y);
    }

    public static BoardIndex operator *(BoardIndex a, BoardIndex b)
    {
        return new BoardIndex(a.X * b.X, a.Y * b.Y);
    }

    public static BoardIndex operator *(BoardIndex a, int b)
    {
        return new BoardIndex(a.X * b, a.Y * b);
    }

    public static bool operator ==(BoardIndex a, BoardIndex b)
    {
        return (a.X == b.X) && (a.Y == b.Y);
    }

    public static bool operator !=(BoardIndex a, BoardIndex b)
    {
        return (a.X != b.X) || (a.Y != b.Y);
    }

}
